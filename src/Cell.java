
public class Cell {
	private int value;
	private boolean visited;
	private boolean wumpus;
	private boolean gold;
	private boolean hole;
	public Cell(int value){
		this.value = value;
		this.visited = false;
		this.wumpus = false;
		this.gold = false;
	}
	public Cell(){
		this.value = 0;
		this.visited = false;
	}	
	public boolean isVisited(){
		return this.visited;
	}
	public void markVisited(){
		this.visited = true;
	}	
	public int getValue(){
		return value;
	}
	public void setValue(int value){
		this.value = value;
	}
	public void addToValue(int value){
		this.value += value;
	}
	public boolean hasWumpus(){
		return this.wumpus;
	}
	public void unMarkWumpus(){
		this.wumpus = false;
	}
	public void markWumpus(){
		this.wumpus = true;
	}
	public boolean hasGold(){
		return this.gold;
	}
	public void markGold(){
		this.gold = true;
	}
	public boolean hasHole(){
		return this.hole;
	}
	public void markHole(){
		this.hole = true;
	}
}
